---
dashboard: Environment metrics
priority: 1
panel_groups:
- group: System metrics (Kubernetes)
  panels:
  - title: Memory Usage (Total)
    type: area-chart
    y_label: Total Memory Used (GB)
    metrics:
    - id: system_metrics_kubernetes_container_memory_total
      query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024
      label: Total (GB)
      unit: GB
      metric_id: 12
  - title: Core Usage (Total)
    type: area-chart
    y_label: Total Cores
    metrics:
    - id: system_metrics_kubernetes_container_cores_total
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)
      label: Total (cores)
      unit: cores
      metric_id: 13
  - title: Memory Usage (Pod average)
    type: line-chart
    y_label: Memory Used per Pod (MB)
    metrics:
    - id: system_metrics_kubernetes_container_memory_average
      query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024
      label: Pod average (MB)
      unit: MB
      metric_id: 14
  - title: Core Usage (Pod Average)
    type: line-chart
    y_label: Cores per Pod
    metrics:
    - id: system_metrics_kubernetes_container_core_usage
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod))     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod_name))
      label: Pod average (cores)
      unit: cores
      metric_id: 16
- group: Response metrics (NGINX Ingress)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_nginx_ingress_16_throughput_status_code
      query_range: sum(label_replace(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]),
        "status_code", "${1}xx", "status", "(.)..")) by (status_code)
      unit: req / sec
      label: Status Code
      metric_id: 18
  - title: Latency
    type: area-chart
    y_label: Latency (ms)
    metrics:
    - id: response_metrics_nginx_ingress_16_latency_pod_average
      query_range: sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_sum{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_count{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 1000
      label: Pod average (ms)
      unit: ms
      metric_id: 19
  - title: HTTP Error Rate
    type: area-chart
    y_label: HTTP Errors (%)
    metrics:
    - id: response_metrics_nginx_ingress_16_http_error_rate
      query_range: sum(rate(nginx_ingress_controller_requests{status=~"5.*",namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 100
      label: 5xx Errors (%)
      unit: "%"
      metric_id: 20
- group: Heap size
  panels:
  - title: Go heap size
    type: area-chart
    y_axis:
      format: 'bytes'
    metrics:
    - metric_id: 'go_memstats_alloc_bytes_1'
      query_range: 'go_memstats_alloc_bytes'
      unit: 'Bytes'
  - title: 'Total consumption'
    type: 'single-stat'
    metrics:
    - id: 10
      query: 'max(go_memstats_alloc_bytes{job="prometheus"})'
      unit: MB
      label: 'Total consumption'
- group: Prometheus HTTP
  panels:
  - type: bar
    title: 'HTTP Handlers'
    x_label: 'Response Size'
    y_axis:
      name: 'Handlers'
    metrics:
    - id: prometheus_http_response_size_bytes_bucket
      query_range: 'sum(increase(prometheus_http_response_size_bytes_bucket[1h])) by (handler)'
      unit: 'Bytes'
  - title: 'Gauge'
    type: 'gauge'
    min_value: 0
    max_value: 1000
    split: 5
    thresholds:
      values: [60, 90]
      mode: 'percentage'
    format: 'kilobytes'
    metrics:
    - id: 10
      query: 'floor(max(prometheus_http_response_size_bytes_bucket)/1000)'
      unit: 'kb'
  - type: area-chart
    title: 'HTTP requests'
    metrics:
    - id: prometheus_http_requests_total_bucket
      query_range: 'prometheus_http_requests_total'
      unit: 'Bytes'
