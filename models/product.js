var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    name : { type: String },
    category : { type: String },
    price : { type: Number },
    nrReserved: { type: Number },
    nrOrdered: { type: Number },
    star: { type: Number},
    nrReview: { type: Number}
});

module.exports = mongoose.model('products', productSchema);
