### Delete all docker containers
#docker system prune -a --volumes

### Stop local mongodb service (systemd & systemv-init) under linux
sudo systemctl stop mongod
sudo service mongod stop

### Compose down then stop all docker containers and remove
docker-compose down
docker ps -a -q | xargs docker stop
docker ps -a -q -f status=exited | xargs docker rm

### Create network and build docker files
docker network create scaly
docker build -f mongo.dockerfile -t mongo .
#docker run -p 27017:27017 --network scaly --name mongo -d mongo

#docker exec -it mongo bash
#  mongo
#    use scalyDB
#    db.createUser({user:"scaly", pwd:"scaly", roles:["readWrite"]})
#    exit
#  exit

docker build --no-cache -f backend.dockerfile -t backend .
#docker run -p 5000:5000 --network scaly --name backend -d backend

cd ../frontend
docker build --no-cache -f frontend.dockerfile -t frontend .
#docker run -p 5046:5046 --network scaly --name frontend -d frontend

### Start docker-compose in backend directory
cd ../backend
docker-compose up --detach
docker-compose start mongoTemp && wait 2 && \
docker-compose start backend && wait 2 && \
docker-compose down
docker-compose up --detach
